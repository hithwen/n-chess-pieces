__author__ = 'hithwen'

'''
Backtraking method: It's really slow and generates duplicated solutions that can be trimmed in the end
                    Constant memory usage
'''

import itertools
from pieces import King, Rook, Knight, Queen, Bishop
from board import Board
from collections import defaultdict


def backtracking(choice_points, choices, assignable, board_size):
    """
    Template method
    choice_points: is a list of the choice points. Each of them will be assigned to a single choice for
                   each solution.
    choices: is a list of choices.
    assignable: is a function that declare if a choice is assignable to a choice point.
          It needs three arguments:
               def assignable(choice, choice_points, solutions):
          In particular, solutions store the assignments of the previous choice points.
          It seems like that {cp0:c0, cp1:c1, ...} where cpI is a choice point and cI is a choice.
    """
    #http://code.activestate.com/recipes/577777-backtracking-template-method/

    N = len(choices)
    M = len(choice_points)

    # solutions is the dict that has for each choice point (key) a choice (value)
    solutions = {}

    cp = 0
    c = 0
    backtrack = False
    end = False

    solutions_found = 0

    while not end:
        #forward
        while not backtrack:
            if assignable(cp, c, solutions, board_size):
                solutions[cp] = c
                if cp == M-1:
                    sol = defaultdict(set)
                    for k, v in solutions.iteritems():
                        sol[choice_points[k]].add(choices[v])
                    solutions_found += 1
                    if solutions_found % 1000 == 0:
                        print '%s solutions found' % solutions_found
                    yield sol
                    del solutions[cp]
                    if not c == N-1:
                        c += 1
                    else:
                        backtrack = True
                else:
                    cp += 1
                    c = 0
            elif c != N-1:
                c += 1
            else:
                backtrack = True

        #backward
        end = (cp == 0)
        while backtrack and not end:
            cp -= 1
            c = solutions.pop(cp)
            if not c == N-1:
                c += 1
                backtrack = False
            elif cp == 0:
                end = True


def solve(n, m, pieces):
    board_size = (n, m)
    board_points = [p for p in itertools.product(range(m), range(n))]

    def assignableNPieces(piece_index, position_index, solutions, board_size):
        x, y = board_points[position_index]
        piece = pieces[piece_index]
        board = Board(numcols=board_size[0], numrows=board_size[1])
        for p_idx, pos_idx in solutions.iteritems():
            placed_piece = pieces[p_idx]
            pos = board_points[pos_idx]
            board.place_piece(pos[0], pos[1], placed_piece)
        can_place = board.can_place_piece(x, y, piece)
        return can_place

    solutions = []
    for s in backtracking(pieces, board_points, assignableNPieces, board_size):
        if s not in solutions:
            solutions.append(s)
    #solutions = set(str(s) for s in backtracking(pieces, board_points, assignableNPieces, board_size))
    return solutions


if __name__ == '__main__':
    n = 7  # columns
    m = 7  # rows
    pieces = (King(), King(), Queen(), Queen(), Bishop(), Knight())
    solution = solve(n, m, pieces)
    for s in solution:
        print s
    print len(solution)



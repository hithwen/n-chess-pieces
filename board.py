__author__ = 'hithwen'

from pieces import Piece
import copy


ENDANGERED = '*'


class Board(object):
    def __init__(self, numrows=None, numcols=None, board=None, pieces=None):
        if numrows is None and numcols is None:
            assert board is not None
            self.numrows = len(board[0])
            self.numcols = len(board)
            self.board = board

        else:
            assert board is None
            self.numrows = numrows
            self.numcols = numcols
            self.board = []
            for i in range(numcols):
                self.board.append([None] * numrows)

        self.pieces = pieces

    def place_piece(self, x, y, piece):
        """ Returns True if is a safe placement """
        if self.board[x][y] is not None:
            return False
        eat = piece.eat_positions(x, y, self.numrows, self.numcols)
        for i, j in eat:
            if isinstance(self.board[i][j], Piece):
                return False
            self.board[i][j] = ENDANGERED
        self.board[x][y] = piece
        return True

    def can_place_next_piece(self, x, y):
        p = self.pieces[-1]
        return self.can_place_piece(x, y, p)

    def can_place_piece(self, x, y, piece):
        if self.board[x][y] is not None:
            return False
        eat = piece.eat_positions(x, y, self.numrows, self.numcols)
        for i, j in eat:
            if isinstance(self.board[i][j], Piece):
                return False
        return True

    def place_next_piece(self, x, y):
        """If possible, places next piece from the ones provided at init time
        Return: if placing was succesful
        """
        p = self.pieces[-1]
        if self.place_piece(x, y, p):
            self.pieces.pop()
            return True
        return False

    def place_next_piece_in_new_board(self, x, y):
        nboard = self.__class__(board=copy.deepcopy(self.board), pieces=copy.copy(self.pieces))
        if nboard.place_next_piece(x, y):
            return nboard

    def __repr__(self):
        s = ''
        for y in range(self.numrows):
            for x in range(self.numcols):
                c = self.board[x][y]
                if isinstance(c, Piece):
                    s += str(c)
                elif c == ENDANGERED:
                    s += ENDANGERED
                else:
                    s += ' '
            s += '\n'
        return s

    def __eq__(self, other):
        return self.board == other.board

    def __hash__(self):
        return hash(tuple(self.placed_pieces()))

    def placed_pieces(self):
        return [x for r in self.board for x in r if isinstance(x, Piece)]


class NonRotableBoard(Board):

    def __eq__(self, other):
        if self.board == other.board:
            return True
        rotated1 = zip(*self.board[::-1])
        rotated1 = [list(elem) for elem in rotated1]
        if rotated1 == other.board:
            return True
        rotated2 = zip(*rotated1[::-1])
        rotated2 = [list(elem) for elem in rotated2]
        if rotated2 == other.board:
            return True
        rotated3 = zip(*rotated2[::-1])
        rotated3 = [list(elem) for elem in rotated3]
        return rotated3 == other.board

    def __hash__(self):
        p = [str(p) for p in self.placed_pieces()]
        s = ''.join(sorted(p))
        return hash(s)

The problem is to find all unique configurations of a set of normal chess pieces on a chess board with dimensions M×N where none of the pieces is in a position to take any of the others.
Assume the colour of the piece does not matter, and that there are no pawns among the pieces.


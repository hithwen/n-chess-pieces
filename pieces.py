from collections import defaultdict


class Piece(object):
    def eat_positions(self, x, y, numrows, numcols):
        """
        :param x: current row
        :param y: current column
        :param numrows: board rows
        :param numcols: board columns
        :return: list of positions the piece could eat
        """
        raise NotImplementedError

    def __eq__(self, other):
        return isinstance(other, self.__class__)

    def __hash__(self):
        return hash(str(self))


class Queen(Piece):
    def eat_positions(self, x, y, numrows, numcols):
        positions = []
        for i, j in zip(range(x+1, numcols), range(y+1, numrows)):
            positions.append((i, j))
        for i, j in zip(range(x-1, -1, -1), range(y-1, -1, -1)):
            positions.append((i, j))
        for i, j in zip(range(x-1, -1, -1), range(y+1, numrows)):
            positions.append((i, j))
        for i, j in zip(range(x+1, numcols), range(y-1, -1, -1)):
            positions.append((i, j))
        positions.extend([(x, j) for j in range(0, numrows) if j != y])
        positions.extend([(i, y) for i in range(0, numcols) if i != x])
        return positions

    def __repr__(self):
        return 'Q'


class King(Piece):
    def eat_positions(self, x, y, numrows, numcols):
        positions = []
        for i in range(x-1, x+2):
            for j in range(y-1, y+2):
                if i >= 0 and j >= 0 and i < numcols and j < numrows \
                    and not (i == x and j == y):
                    positions.append((i, j))
        return positions

    def __repr__(self):
        return 'K'


class Rook(Piece):
    def eat_positions(self, x, y, numrows, numcols):
        positions = [(x, j) for j in range(0, numrows) if j != y]
        positions.extend([(i, y) for i in range(0, numcols) if i != x])
        return positions

    def __repr__(self):
        return 'R'


class Knight(Piece):
    def eat_positions(self, x, y, numrows, numcols):
        positions =  [(x-1, y-2), (x+1, y-2), (x+2, y-1), (x+2, y+1),
                       (x+1, y+2), (x-1, y+2), (x-2, y+1), (x-2, y-1)]
        positions = [(x, y) for x, y in positions if x >= 0 and y >= 0
                     and x < numcols and y < numrows]
        return positions

    def __repr__(self):
        return 'N'


class Bishop(Piece):
    def eat_positions(self, x, y, numrows, numcols):
        positions = []
        for i, j in zip(range(x+1, numcols), range(y+1, numrows)):
            positions.append((i, j))
        for i, j in zip(range(x-1, -1, -1), range(y-1, -1, -1)):
            positions.append((i, j))
        for i, j in zip(range(x-1, -1, -1), range(y+1, numrows)):
            positions.append((i, j))
        for i, j in zip(range(x+1, numcols), range(y-1, -1, -1)):
            positions.append((i, j))
        return positions

    def __repr__(self):
        return 'B'
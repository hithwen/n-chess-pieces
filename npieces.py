'''
This implements a width-first algorightm
'''
from board import NonRotableBoard
from pieces import King, Rook, Knight, Queen, Bishop
import copy
import itertools


def generate_solution(initial_solutions, rows, columns):
        solutions = initial_solutions
        while any([x.pieces for x in solutions]):  # Cannot assume there exactly one piece in every row or column
            print 'Exploring %s solutions' % len(solutions)
            solutions = set(board.place_next_piece_in_new_board(i, j)
                            for board in solutions
                            for i in range(columns)
                            for j in range(rows)
                            if board.can_place_next_piece(i, j)
                            )
        print 'Encountered %s non-rotated solutions' % len(solutions)
        return solutions


def solve(n, m, pieces):
    solutions = [NonRotableBoard(numcols=n, numrows=m, pieces=list(pieces))]
    solutions = generate_solution(solutions, columns=n, rows=m)
    solution_list = []
    for solution in solutions:
        solution_list.append(solution)
        rotated1 = zip(*solution.board[::-1])
        rotated1 = [list(elem) for elem in rotated1]
        if rotated1 != solution.board:
                solution_list.append(NonRotableBoard(board=rotated1))
        rotated2 = zip(*rotated1[::-1])
        rotated2 = [list(elem) for elem in rotated2]
        if rotated2 != solution.board and rotated2 != rotated1:
                solution_list.append(NonRotableBoard(board=rotated2))
        rotated3 = zip(*rotated2[::-1])
        rotated3 = [list(elem) for elem in rotated3]
        if rotated3 != solution.board and rotated3 != rotated1 and rotated3 != rotated2:
                solution_list.append(NonRotableBoard(board=rotated3))
    return solution_list


if __name__ == '__main__':
    answers = solve(7, 7, (King(), King(), Queen(), Queen(),
                           Bishop(), Bishop(), Knight()))
    for a in answers:
        print str(a)
    print len(answers)

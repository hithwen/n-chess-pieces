__author__ = 'hithwen'

import unittest
from pieces import Queen, King, Knight


class QueenTest(unittest.TestCase):
    def setUp(self):
        self.queen = Queen()

    def test_eat(self):
        eat = self.queen.eat_positions(0, 0, 4, 4)
        self.assertEquals(eat, [(1, 1), (2, 2), (3, 3),
                                (0, 1), (0, 2), (0, 3),
                                (1, 0), (2, 0), (3, 0)])

        eat = self.queen.eat_positions(0, 0, 3, 3)
        self.assertEquals(eat, [(1, 1), (2, 2),
                                (0, 1), (0, 2),
                                (1, 0), (2, 0)])

        eat = self.queen.eat_positions(2, 1, 3, 5)
        self.assertEquals(set(eat),
                          set([(1, 0), (3, 2), (1, 2), (3, 0),
                               (2, 0), (2, 2),
                               (0, 1), (1, 1), (3, 1), (4, 1)]))


class KingTest(unittest.TestCase):
    def test_eat(self):
        k = King()
        eat = k.eat_positions(2, 2, 4, 4)
        self.assertEquals(set(eat),
                          set([(1, 3), (1, 1), (1,2),
                               (2, 1), (2,3),
                               (3, 3), (3, 1), (3, 2)]))


class KnightTest(unittest.TestCase):
    def test_eat(self):
        n = Knight()
        eat = n.eat_positions(0, 2, numcols=7, numrows=3)
        self.assertEquals([(1, 0), (2, 1)], eat)
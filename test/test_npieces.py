__author__ = 'hithwen'

import unittest
from npieces import solve
from pieces import Queen, King, Rook, Knight


class TestNPieces(unittest.TestCase):
    def test_num_solutions_1_queens(self):
        solutions = solve(1, 1, [Queen()])
        self.assertEquals(1, len(solutions))

    def test_num_solutions_2_queens(self):
        solutions = solve(2, 2, [Queen()] * 2)
        self.assertEquals(0, len(solutions))

    def test_num_solutions_3_queens(self):
        solutions = solve(3, 3, [Queen()] * 3)
        self.assertEquals(0, len(solutions))

    def test_num_solutions_4_queens(self):
        solutions = solve(4, 4, [Queen()] * 4)
        self.assertEquals(2, len(solutions))

    def test_num_solutions_5_queens(self):
        solutions = solve(5, 5, [Queen()] * 5)
        self.assertEquals(10, len(solutions))

    def test_kings_rook(self):
        solutions = solve(3, 3, [King(), King(), Rook()])
        self.assertEquals(4, len(solutions))

    def test_rooks_knights(self):
        solutions = solve(4, 4, [Knight(), Knight(), Knight(),
                                 Knight(), Rook(), Rook()])
        self.assertEquals(8, len(solutions))

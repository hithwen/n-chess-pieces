__author__ = 'hithwen'

import unittest
from board import Board, NonRotableBoard
from pieces import King, Rook


class BoardTest(unittest.TestCase):
    def test_repr(self):
        b = Board(3, 3)
        b.place_piece(0, 0, King())
        b.place_piece(2, 1, Rook())
        b.place_piece(0, 2, King())
        self.assertEquals('K**\n**R\nK**\n', str(b))

    def test_repr2(self):
        b = Board(numcols=7, numrows=3)
        b.place_piece(4, 1, King())
        self.assertEquals('   *** \n   *K* \n   *** \n', str(b))

        b = Board(board=b.board)
        self.assertEquals('   *** \n   *K* \n   *** \n', str(b))


class NonRotableBoardTest(unittest.TestCase):
    def test_equals(self):
        b1 = NonRotableBoard(3, 3)
        b1.place_piece(0, 0, King())
        b1.place_piece(2, 1, Rook())
        b1.place_piece(0, 2, King())

        b2 = NonRotableBoard(3, 3)
        b2.place_piece(0, 0, King())
        b2.place_piece(1, 2, Rook())
        b2.place_piece(2, 0, King())

        self.assertEquals(hash(b1), hash(b2))
        self.assertEquals(b1, b2)

